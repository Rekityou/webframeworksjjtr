import conexion from "./conexion.js";
import { rejects } from "assert";

var alumnosDb = {}
alumnosDb.insertar = function insertar(alumno){
    return new Promise((resolve,rejects)=>{

        let sqlConsulta = "Insert into alumnos set ?";
        conexion.query(sqlConsulta,alumno,function(err,res){

            if(err){
                console.log("Surgio un error ",err.message);
                rejects(err);
            }
            else {
                const alumno = {
                    id:res.id,
                }
                resolve(alumno);
            }

        });
    });
}

alumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject)=> {

        var sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta, null, function(err, res) {

            if(err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }

        });

    });
}

alumnosDb.consultarMatricula = function consultarMatricula(matricula) {

    return new Promise((resolve, reject) => {
        var sqlConsulta = "SELECT * FROM alumnos WHERE matricula = ?";
        conexion.query(sqlConsulta, [matricula], function(err, res){

            if(err) {
                console.log("Surgio un error: ", err.message);
                reject(err);
            } else {
                resolve(res);
            }

        });
    });

}

alumnosDb.borrarMatricula = function borrarMatricula(matricula) {

    return new Promise((resolve, reject) => {
        var sqlConsulta = "DELETE FROM alumnos WHERE matricula = ?";
        conexion.query(sqlConsulta, [matricula], function(err, res){

            if(err) {
                console.log("Surgio un error: ", err.message);
                reject(err);
            } else {
                resolve(res);
            }

        });
    });

}

alumnosDb.actualizarDatos = function actualizarDatos(matricula) {

    return new Promise((resolve, reject) => {
        var sqlConsulta = "UPDATE alumnos SET nombre = ?, domicilio = ?, especialidad = ?, sexo = ? WHERE matricula = ?";
        conexion.query(sqlConsulta, [nombre, domicilio, especialidad, sexo, matricula], function(err, res){

            if(err) {
                console.log("Surgio un error: ", err.message);
                reject(err);
            } else {
                resolve(res);
            }

        });
    });

}

export default alumnosDb;